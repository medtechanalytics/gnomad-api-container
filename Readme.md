# Docker Image for gnomAD API

This project creates a Docker image for the [gnomAD API](https://github.com/macarthur-lab/gnomadjs/tree/master/projects/gnomad-api).

### Environment variables

Override these default settings as needed:

```
ELASTICSEARCH_URL="http://localhost:8001/api/v1/namespaces/default/services/elasticsearch:9200/proxy"
REDIS_HOST="localhost"
GRAPHQL_PORT=8007
```

### Run the pre-built image

If you prefer to use the pre-built container

```bash
docker run -p 127.0.0.1:8087:8007 registry.gitlab.com/medtechanalytics/gnomad-api-container
```


### Build the image, if you prefer

Build and run the docker image from the project's `Dockerfile`:

```bash
docker build -t gnomad/api .
docker run -p 127.0.0.1:8087:8007 gnomad/api
```

