FROM node:10.11.0

RUN apt-get update && apt install -y git
RUN npm install -g webpack webpack-cli
RUN mkdir /var/app
RUN cd /var/app && git clone --recursive https://github.com/macarthur-lab/gnomadjs.git
RUN cd /var/app/gnomadjs && git checkout 16530a1df2cf45dc52e3ea884173720b6135c108
RUN cd /var/app/gnomadjs/projects/gnomad-api && npm install --production
RUN cd /var/app/gnomadjs/projects/gnomad-api && npx webpack

WORKDIR /var/app/gnomadjs/projects/gnomad-api/

ENV ELASTICSEARCH_URL="http://localhost:8001/api/v1/namespaces/default/services/elasticsearch:9200/proxy"
ENV REDIS_HOST="localhost"
ENV GRAPHQL_PORT=8007

EXPOSE 8007

CMD ["node", "dist/server.js"]
